

The file abc.txt contains the following 3 lines of text:

1. abc
2. def
3. ghi

What is the output of the following snippet of code in abc.py file?
1. file = open('abc.txt', ‘w')

2. file.close()

3. file = open('abc.txt')

4. print(file.read())

A. An error occurs

B. The output is:

1. None

C. The output is:

1. abc
2. def
3. ghi

D. An empty string is output

E. The output is:

1. abc


