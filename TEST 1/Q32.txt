Question 32 of 40

Which of the following is a true statement? (Select 2 Answers)

A. In UTF-8, the 8 represents 8 languages
B. ASCII stands for American standard code for information interchange
C. ASCII and UTF-8 are mutually exclusive

D. ASCII is a subset of UTF-8

E. ASCII has more code points as compared to UTF-8
