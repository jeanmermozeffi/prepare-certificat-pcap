Question 35 of 40

What is the output of the following snippet of code?

1. class A:
2. pass

3. class B(A):
4. pass

5. class C(B):
6. pass

7.

8. c=C()

9. print(isinstance(c, A))

A. A
B. TypeError
C. False

D. Yes

E. True

