Question 31 of 40

What is the output of the following code snippet? (Select 2 Answers)
print(ord('c'))

A. The output will be an integer
B. The output is ‘d'
C. The output will be a string
D. The output is a Unicode code point

E. An error will occur
