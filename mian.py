# for i in range(-1, 1):
#     print("#")

# z = 10
# y = 0
# x = z > y or z == y
# print(x)

# my_list = [3, 1, -1]
# my_list[-1] = my_list[-2]
# print(my_list)

# my_list = [0 for i in range(-1, 2)]
# print(my_list)

# i = 0
# while i <= 5:
#     i += 1
#     if i % 2 == 0:
#         break
#     print("*")

# vals = [0, 1, 2]
# vals.insert(0, 1)
# del vals[1]
# print(vals)

# x = 1
# x = x == x

# print(x)

# t = [[3-i for i in range(3)] for j in range(3)]
# s = 0
# for i in range(3):
#     s += t[i][i]
# print(s)

# var = 0
# while var < 6:
#     var += 1
#     if var % 2 == 0:
#         continue
#     print(var)
#     print("#")

# for i in range(1):
#     print("#")
# else:
#     print("#")

# i = 0
# while i <= 3:
#     i += 2
#     print("*")

# my_list_1 = [1, 2, 3]
# my_list_2 = []
# for v in my_list_1:
#     my_list_2.insert(0, v)
# print(my_list_2)

# my_list = [1, 2, 3, 4]
# print(my_list[-3:-2])

# var = 1
# while var < 10:
#     print("#")
#     var = var << 1
#     print(var)

# a = 1
# b = 0
# c = a & b
# d = a | b
# e = a ^ b

# print(c + d + e)

# z = 10
# y = 0
# x = y < z and z > y or y > z and z < y
# print(x)

# my_list = [[0, 1, 2, 3] for i in range(2)]
# print(my_list[2][0])
# my_list = [3, 1, -2]
# print(my_list[my_list[-1]])

# vals = [0, 1, 2]
# vals.insert(0, 1)
# del vals[1]

# print(vals)

# vals = [0, 1, 2]
# vals[0], vals[2] = vals[2], vals[0]

# print(vals)

# tup = (1, ) + (1, )
# tup = tup + tup
# print(len(tup))

# try:
#     first_prompt = input("Enter the first value: ")
#     a = first_prompt
#     second_prompt = input("Enter the second value: ")
#     b = len(second_prompt) * 2
#     print(a/b)
# except ZeroDivisionError:
#     print("Do not divide by zero!")
# except ValueError:
#     print("Wrong value.")
# except:
#     print("Error.Error.Error.")

# x = input()
# y = input()

# x = x + y

# print(x)

# x = int(input())
# y = int(input())

# x = x % y
# x = x % y
# y = y % x

# print(y)

# x = 1 / 2 + 3 // 3 + 4 ** 2
# print(10 % 0)
# print(1 // 2 * 3)
# print(1 / 1)
# z = y = x = 1
# print(x, y, z, sep='*')
# print("bonjour\n")
# def fun(x, y, z):
#     return x + 2 * y + 3 * z


# print(fun(0, z=1, y=3))
# v = None
# print(v)

# def func_1(a):
#     return a ** a
# def func_2(a):
#     return func_1(a) * func_1(a)
# print(func_2(2)) //16

# def fun(x):
#     global y
#     y = x * x
#     return y
# fun(2)
# print(y) //16

# def fun(x):
#     x += 1
#     return x


# x = 2
# x = fun(x + 1)
# print(x) //4

# def fun(inp=2, out=3):
#     return inp * out


# print(fun(out=2)) //4

# my_list = ['Mary', 'had', 'a', 'little', 'lamb']


# def my_list(my_list):
#     del my_list[3]
#     my_list[3] = 'ram'


# print(my_list(my_list)) //  Error

# def fun(x):
#     if x % 2 == 0:
#         return 1
#     else:
#         return


# print(fun(fun(2)) + 1) // Error

# tup = (1, 2, 4, 8)
# tup = tup[1:-1]
# tup = tup[0]
# print(tup) //Error

# def any():
#     print(var + 1, end='')


# var = 1
# any()
# print(var) // 21

# def f(x):
#     if x == 0:
#         return 0
#     return x + f(x - 1)
# print(f(3)) //6


# for i in range(1, -1, -1):
#     try:
#         print(1/i, end="")
#     except:
#         print("ERROR", end=" ")
#     else:
#         print("SAFE", end=" ")
#     finally:
#         print("DONE", end="")

# class A:
#     def _init__(self, a=1):
#         self.__var = a


# a = A(5)
# print(a._A__var)
# a = "television"
# print(a.find('Vision'))

# a = "ab cd"

# print('*'.join(a.split()))

# a = [1, 2, 3, 4, 5, 6]
# b = list(filter(lambda x: x % 2 == 0, a))

# print(b)

# class A:
#     pass


# class B(A):

#     pass


# class C(B):

#     pass


# c = C()

# print(isinstance(c, A))


# import platform as p
# print(p.python_version())

# import math

# print(dir(math))

# a = "abcdef"

# a[1] = 'x'

# print(a)

# import math as m

# print(m.ceil(-2.3) + m.factorial(3))

# a = "abc"
# for i in a:
#     a += 'z'
#     print(a)

# class A:
#     pass


# print(A.__module__)

# class A:
#     var = 100

#     def __init__(self):
#         pass


# a = A()
# print(a.__dict__)

# def func(a):
#     print(a)


# func()

# class A:

#     pass


# class B(A):

#     pass


# class C(B):

#     pass


# c = C()

# print(isinstance(c, A)) // True


# print(C.__bases__)

# print(ord('c'))
# a = "abcdef"

# b = a[-3:-1]

# print(b)

# x = 4

# print(x(2))

# a = "refrigerator"

# print(a.find('e'))

# def A(a):
#     b = a**2

#     def B(b):
#         return b+3
#     return B


# x = A(2)
# print(x(2))

# class A:
#     def func(self):
#         return "A"


# class B:
#     def func(self):
#         return "B"


# class C(B, A):
#     pass


# c = C().func()

# print(c)

# lst = [i for i in range(-1, -2)]
# print(lst)

# foo = (1, 2, 3)
# print(foo.index(0))

# my_list = [x * x for x in range(5)]


# def fun(lst):
#     del lst[lst[2]]
#     return lst


# print(fun(my_list))

# x = int(input())
# y = int(input())
# x = x % y
# x = x % y
# y = y % x
# print(y)

# nums = [1, 2, 3]
# vals = nums
# print(vals)
# print(nums)

# x = 1
# y = 2
# x, y, z = x, x, y
# z, y, z = x, y, z

# print(x, y, z)

# nums = [1, 2, 3]
# vals = nums
# del vals[:]
# print(nums, vals)

# def fun(inp=2, out=3):
#     return inp * out


# print(fun(out=2))

# def function_1(a):
#     return None
# def function_2(a):
#     return function_1(a) * function_1(a)
# print(function_2(2))

# dct = {}
# dct['1'] = (1, 2)
# dct['2'] = (2, 1)

# for x in dct.keys():
#     print(dct[x][1], end="")

# lst = [[x for x in range(3)] for y in range(3)]

# for r in range(3):
#     for c in range(3):
#         if lst[r][c] % 2 != 0:
#             print("#")

# print(Hello, World!)

# try:
#     value = input("Enter a value: ")
#     print(int(value)/len(value))
# except ValueError:
#     print("Bad input...")
# except ZeroDivisionError:
#     print("Very bad input...")
# except TypeError:
#     print("Very very bad input...")
# except:
#     print("Booo!")

# print("a", "b", "c", sep="sep")

# dd = {"1": "0", "0": "1"}
# for x in dd.vals():
#     print(x, end="")

# x = float(input())
# y = float(input())
# print(y ** (1 / x))

# print(1 // 2)

# try:
#     print(5/0)
#     break
# except:
#     print("Sorry, something went wrong...")
# except (ValueError, ZeroDivisionError):
#     print("Too bad...")

# tup = (1, 2, 4, 8)
# tup = tup[-2:-1]
# tup = tup[-1]
# print(tup)

# my_list = [1, 2]

# for v in range(2):
#     my_list.insert(-1, my_list[v])

# print(my_list)

# a = 1
# b = 0
# a = a ^ b
# b = a ^ b
# a = a ^ b

# print(a, b)

# x = 1 // 5 + 1 / 5
# print(x) // 0.2

# def fun(x):
#     if x % 2 == 0:
#         return 1
#     else:
#         return 2


# print(fun(fun(2))) //2

# i = 0
# while i < i + 2:
#     i += 1
#     print("*")
# else:
#     print("*")

# dct = {'one': 'two', 'three': 'one', 'two': 'three'}
# v = dct['three']

# for k in range(len(dct)):
#     v = dct[v]

# print(v) // one

# def fun(x, y):
#     if x == y:
#         return x
#     else:
#         return fun(x, y-1)


# print(fun(0, 3))

# z = 0
# y = 10
# x = y < z and z > y or y > z and z < y
# print(x)

# my_list = [x * x for x in range(5)]

# print(my_list)

# ************************************************************** # 
## TEST PCAP 5
# ************************************************************** # 
# from platform import python_implementation
# print(python_implementation())
#CPython
# ************************************************************** # 
# a = "abcdef"
# del a
# print(a) # Error message

# ************************************************************** # 
a = "qwecvxqwe"
# print(a.index('qwe') - a.rfind('qwe')) # 0-6 = -6

# ************************************************************** # 
# print(chr(70)) F

# ************************************************************** # 
# print("apple vs/"samsung/"") # Error

# ************************************************************** # 
# try:
#     print(i = 1/0)
# except TypeError as e:
#     print("Hi!", end= "\n")
# finally:
#     print("Bye!")

# ************************************************************** # 
# a = "abcdef"
# b = a[:] # "abcdef"
# print(b)

# ************************************************************** # 
# import math as m
# print(m.sqrt(16) - m.hypot(4,3)) #4-5

# ************************************************************** # 
# class A:
#     __Var = 100
#     def __init__(self):
#         A._A__Var += 1
 
# a = A()
# b = A()
# print(A()._A__Var) #103

# ************************************************************** # 
# a = "refrigerator"
# print(a.rfind('r', 0, 9)) #7

# ************************************************************** # 
# a = "abcdef"
# b = a[:-2:2]
# print(b) #ac


# ************************************************************** # 
# a = "engineer"
# print(a.rfind('e',0,-3)) #0

# ************************************************************** # 
# a = [1,2,3,4,5,6]
# del a[0:2]
# print(a)


# ************************************************************** # 
# def A(a):
#     b = a**3

#     def B():
#         return b+2

#     return B

# x = A(1)

# print(x())

# A.
# An error occurs 

# B.
# 6

# C.
# 3 True

# D.
# 0

# E.
# 5

# ************************************************************** # 
# Which of the following evaluates to False given the following code? (Select 2 Answers)

# a = "referee"
# A.
# a.index('e', 2) == 1 # False

# B.
# a.index('e', 2) == 3 # True

# C.
# a.index('r') == 4 # False

# D.
# a.index('r') == 0 True
# E.
# a.index('efe') == 1 True
# ************************************************************** # 


# [print('*' * i) for i in range(6)]
# ************************************************************** # 
# import random as r
# r.seed(0)
# for i in range(3):
#     print(r.random())

# Output:
# 0.8444218515250481 ## Corret
# 0.7579544029403025
# 0.420571580830845

# What is the output when the following code is run on a different machine?
# import random as r
# r.seed(0)
# print(r.random())

# ************************************************************** # 
# a = "mangouaeffi"
# b = a[::2]
# c = a[::3]
# print(b) #mnoafi
# print(c) #mgaf

# a = "abcdef"
# b = a[-3:: -2]
# print(b) # 'db'

# ************************************************************** # 
# class A:
#     __var = 100
#     def __init__(self):
#         self.__var = 10
    
#     def get_var(self):
#         return self._A__var
# a = A()
# print(a.get_var()) #10
# print(A().get_var()) #10

# ************************************************************** # 
# [print(i, end='-') if i%5 == 0 else print('', end='') for i in range(20)]
# ************************************************************** # 


# class A:
#     var = 1
#     def __init__(self):
#         self.x = 1
# a = A()
# print(hasattr(a, 'x'))

# print(hasattr(a, 'var'))
# print(hasattr(A, 'var'))
# print(hasattr(A(), 'x'))

# ************************************************************** # 

# def func(p1, p2):
#     p1 = 1
#     p2[0] = 42
# x = 3
# y = [1, 2, 3]

# func(x, y)
# print(x, y[0])

# import math

# result = math.e != math.pow(2, 4)
# print(int(result))

# from random import randint

# for i in range(2):
#     print(randint(1, 2), end='')

# try:
#     raise Exception
# except:
#     print("c")
# except BaseException:
#     print("a")
# except Exception:
    # print("b")
# for line in open('text.txt', 'rt'):
    
# x = "\\\"
# print(len(x))

# print(float("1.3"))

# class Class:
#     def __init__(self, val=0):
#         pass


# object = Class(1, 2)

# class A:
#     def __init__(self, v=2):
#         self.v = v
 
#     def set(self, v=1):
#         self.v += v
#         return self.v
 
 
# a = A()
# b = a
# b.set()
# print(a.v)

# class A:
#     A = 1
#     def __init__(self):
#         self.a = 0
 
 
# print(hasattr(A, 'a'))

# class A:
#     pass
 
 
# class B(A):
#     pass
 
 
# class C(B):
#     pass
 
 
# print(issubclass(A, C))


# class A:
#     def __init__(self, v):
#         self.__a = v + 1
# a = A(0)
# print(a.__a)

# class A:
#     def __init__(self):
#         pass
# a = A(1)
# print(hasattr(a, 'A'))

# class A:
#     def a(self):
#         print('a')
 
 
# class B:
#     def a(self):
#         print('b')
 
 
# class C(B, A):
#     def a(self):
#         self.a()
# o = C()
# o.c()

# try:
#     raise Exception(1, 2, 3)
# except Exception as e:
#      print(len(e.args))

# def my_fun(n):
#     s = '+'
#     for i in range(n):
#         s += s
#         yield s
        
# for x in my_fun(2):
#     print(x, end='')

# class I:
#     def __init__(self):
#         self.s = 'abc'
#         self.i = 0
 
#     def __iter__(self):
#         return self
 
#     def __next__(self):
#         if self.i == len(self.s):
#             raise StopIteration
#         v = self.s[self.i]
#         self.i += 1
#         return v
 
 
# for x in I():
#     print(x, end='')

# def o(p):
#     def q():
#         return '*' * p
#     return q
 
 
# r = o(1)
# s = o(2)
# print(r() + s())

# s = "stream"
# q = s.read(1)
# print(q)

# numbers = [i*i for i in range(5)]
# foo = list(filter(lambda x: x % 2, numbers))
# print(foo)

# import random
 
# a = random.randint(0, 100)
# b = random.randrange(10, 100, 3)
# c = random.choice((0, 100, 3))
# print(a, b, c)

# import os
 
# os.mkdir('pictures')
# os.chdir('pictures')
 
# print(os.getcwd())

# from datetime import datetime
 
# datetime_1 = datetime(2019, 11, 27, 11, 27, 22)
# datetime_2 = datetime(2019, 11, 27, 0, 0, 0)
 
# print(datetime_1 - datetime_2)

# from datetime import timedelta
 
# delta = timedelta(weeks = 1, days = 7, hours = 11)
# print(delta * 2)

# import calendar
 
# calendar.setfirstweekday(calendar.SUNDAY)
# print(calendar.weekheader(3))




# lst = [1, 2, 3, 4]
# lst = lst[-3:-2]
# print(lst)

# for i in range(1, 3):
#     print("*", end="")
# else:
#     print("*")

# strTest = str(1/3)
# print(strTest)
# d = ''

# for i in strTest:
#     d = d + i
# print(d)
# print(d[-1])

# print(len("kangaroo"))
x = [[[1, 2], [3, 4]], [[5, 6], [7, 8]]]

# def func(data):
#     res = data[0][0]
#     for da in data:
#         print(da)
#         for d in da:
#             print(d)
#             if res < d:
#                 res = d
#                 print('res', res)
#     return res

# print(func(x[0]))

# x = '\''
# print(len(x))


# print(ord("0") - ord("9") == 10)

for i in range(2):
    print(i)