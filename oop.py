class BasketballTeam:
    def __init__(self, team_name, wins, losses) -> None:
        self.team_name = team_name
        self.wins = wins
        self.losses = losses
        
    def get_team_stats(self):
        return f"[Basketbal] Statistics: {self.team_name}: {self.wins} Victoires - {self.losses} Défaites"
        
        
team_1 = BasketballTeam("Golden States Warios", 13, 0)
team_2 = BasketballTeam("Los Angeles Lakers", 45, 23)

print(team_1.get_team_stats())
print(BasketballTeam.get_team_stats(team_2))