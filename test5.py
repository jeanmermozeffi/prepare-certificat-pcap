# a = "abcdef"

# b = a[:-1]

# print(b)

# a = [1, 2, 3, 4, 5, 6]

# del a[0:2]

# print(a)

# class A:
#     def __init__(self, var):
#         self.var = var


# a = A(10)

# b = A(100)

# print(b.var)

# a = "engineer"

# print(a.rfind('e', 0, -3))

# a = "abcdef"
# del a
# print(a)

# a = "refrigerator"
# print(a.rfind('r', 0, 9))

# a = ['aBc', 'Def', 'gHi']

# b = list(map(lambda x: x.capitalize(), a))

# print(b)

# a = "abcdef"

# b = a[:-2:2]

# print(b)

# class A:
#     def __init__(self):
#         self.var = 10

#         pass


# print(A.__dict__)

# from platform import python_implementation
# print(python_implementation())

# try:
#     print(i=1/0)
# except TypeError as e:
#     print("Hi!", end="\n")
# finally:
#     print("Bye!")

# a = "ab cd:ef"

# print(a.split(':'))

# print(float("abc"))

# [print(i, end='-') if i % 5 == 0 else print('', end='') for i in range(20)]

# a = "abcdef"

# b = a[:]

# print(b)

# print(chr(70))
# def a(x, y): return x*y
# def b(n): return n+1


# print(a(b(1), b(2)))

# a = "qwecvxqwe"
# print(a.index('qwe') - a.rfind('qwe'))

# a = "abcdef"

# b = a[::3]

# print(b)

import math as m
print(m.sqrt(16) - m.hypot(4, 3))
