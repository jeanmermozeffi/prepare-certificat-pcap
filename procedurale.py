# class Voiture:
#     def __init__(self):
#         self.nom = "Ferrari"
#         self.prenom = "Pegeot"

#     def donne_moi_le_modele(self):
#         return "250"


# ma_voiture = Voiture()

# print(ma_voiture.__dict__)


def pluralyze(total, single, plural=None):
    assert isinstance(total, int) and total >= 0, "La valeur doit être une valeur intière positive"

    if plural is None:
        plural = single + 's'

    string = plural if total <= 0 else single

    return print(f'{total}, {string}')


def get_footbal_team_stats(teams_name, wins, losses):
    return f"[FOOTBALL] Statistics: {teams_name}: {pluralyze(wins, 'victoire')}, {pluralyze(losses, 'défaite')}"

def get_basketbal_team_stats(teams_name, wins, losses):
    return f"[Basketbal] Statistics: {teams_name}: {pluralyze(wins, 'victoire')}, {pluralyze(losses, 'défaite')}"




if __name__ == '__main__':
    #Basketball Team
    print(get_basketbal_team_stats('TEK-UP Internationnal Club', 12, 1))
    
    #Football Team
    print(get_footbal_team_stats('TEK-UP Internationnal Club', 1, 15))

    rapport_stats = "Toronto Repports Statistics-36-14"
    
    data = rapport_stats.split('-')
    
    print(get_basketbal_team_stats(data[0], int(data[1]), int(data[2])))
    
    with open("toronto.txt") as file:
        data = file.readline().strip().split('-')
        print(get_footbal_team_stats(data[0], int(data[1], int(data[2]))))
    